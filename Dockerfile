FROM python:3.12

# Copy dependencies
WORKDIR /app
COPY pyproject.toml ./

# Install poetry
RUN pip install poetry

RUN poetry config virtualenvs.create false  \
    && poetry install

COPY . /app
