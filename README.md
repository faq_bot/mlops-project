## Content

- [Setting up the environment](#setting-up-the-environment)

- [Linters and code formatters](#linters-and-code-formatters)

- [Pre-commit](#pre-commit)

- [Methodology](#methodology)

## Setting up local environment
Firstly you need to install poetry and activate virtual environment in the project folder as follows:
```
pipx install poetry==1.8.2

poetry shell
```

Then you need to install all the dependecies from pyproject.toml file by passing:
```
poetry install
```

Since project use python ^3.11 you can run (it helps not to see "trying to find and use a compatible version" every time): 

```
poetry env use python3.11
```

To run the project file you can simply run:

```
poetry run python mlops_project/main.py
```

To exit the virtual environment you need to use:

```
poetry exit
```

If you need more information about interaction with poetry you can check basic usage [guide](https://python-poetry.org/docs/basic-usage/)

### git lfs
It's git lfs used to store the data (for simplicity). A (one-line) guide is here: https://git-lfs.com/

The only issue one might encounter is locked files. The dataset in data/raw is read-only and should not be edited (by methodology). But in case one needs to update it, should check the guide:
https://github.com/git-lfs/git-lfs/wiki/File-Locking

(Also check .gitattributes)

## Run Docker

To build Docker image from Dockerfile you need to run: 

```
docker build .
```
To run the container you can use: 

```
docker run -dp 127.0.0.1:8888:8888 faq

```

To start container you can use: 

```
docker compose up
```

To run commands in working container you can use: 

```
docker compose exec -it faq bash
```



## Linters and code formatters
As a linter and code formatter I decided to use [Ruff](https://github.com/astral-sh/ruff). It is extremely fast and covers a gentleman's set of commonly used Flake8, Black & Sort. Also it supports `pyproject.toml` and supports autocompletion in poetry shell.

To check what configuration I used for ruff check `pyproject.toml`

## Pre-commit
As pre-commit hooks I have to used:
* `trailing-whitespace` - trims trailing whitespace
* `end-of-file-fixer` - ensures that a file is either empty, or ends with one newline
* `check-added-large-files` - prevents giant files from being committed
* `check-merge-conflict` - checks for files that contain merge conflict strings
* `mirrors-mypy` - static type checker
* `ruff-pre-commit` - linter and formatter

## Methodology

For our project we will work with two repos. First is devoloping repository with the methodology similar to gitflow. We decide to simplify the workflow's tree to three type of branches: main, develop and feature, so it looks like this: 


<img src="https://wac-cdn.atlassian.com/dam/jcr:34c86360-8dea-4be4-92f7-6597d4d5bfae/02%20Feature%20branches.svg?cdnVersion=1539" width=500>


We have feature branches that uses develop as a parent branch. When feature is complete we merge it back into the develop branch. Also you need to notice that we do not have release branch and uses develop branch for tests also. After tests have been passed in develop branch, it is merged into the main and tagged with a version number.

Second repo can be called as "data engineer" repo. We need this additional repo for data processing. The methodology used for this repo will be the same. This repo will be created soon. 