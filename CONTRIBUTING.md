## Content

- [How to use linters and code formatters](#how-to-use-linters-and-code-formatters)

- [How to use Pre-commit](#how-to-use-pre-commit)

## How to use linters and code formatters

Once Ruff installed, you can run Ruff on your files by:
```
ruff check   # Lint all files in the current directory.
ruff format  # Format all files in the current directory.
```
Or specify the directory by:
```
ruff format                   # current directory

ruff format path/to/code/     # `path/to/code` (and any subdirectories)

ruff format path/to/file.py   # Format a single file.
```

## How to use Pre-commit

In your shell you need to specify:

```
poetry run pre-commit install
```

To check files you need to run:
```
poetry run pre-commit run --all-files
poetry run pre-commit run --files path/to/file
```
